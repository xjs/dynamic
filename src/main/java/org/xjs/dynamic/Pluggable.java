package org.xjs.dynamic;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Felix Resch on 29-Apr-16.
 */
public interface Pluggable<J extends Pluggable> {

    Collection<Object> __children();

    default <T> Optional<T> getFirst(Class<T> type) {
        Collection<Object> children = __children();
        return children.stream().filter(c -> type.isAssignableFrom(c.getClass())).map(type::cast).findFirst();
    }

    default <T> List<T> get(Class<T> type) {
        Collection<Object> children = __children();
        return children.stream().filter(c -> type.isAssignableFrom(c.getClass())).map(type::cast).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    default <T> J removeAll(Class<T> type) {
        Iterator iterator = __children().iterator();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            if(type.isAssignableFrom(object.getClass())) {
                iterator.remove();
            }
        }
        return (J) this;
    }

    @SuppressWarnings("unchecked")
    default J reset() {
        __children().clear();
        return (J) this;
    }

    @SuppressWarnings("unchecked")
    default <T> J add(T object) {
        __children().add(object);
        return (J) this;
    }

}
